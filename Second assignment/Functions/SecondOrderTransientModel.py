
# coding: utf-8

# In[ ]:


class SecondOrderTransientModel:
    """Class defining a first order implicit transient model"""

    def __init__(self, grid, T, Told, Too, rho, cp, dt):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Told = Told
        self._Too = Too
        self._rho = rho
        self._cp = cp
        self._dt = dt

    def add(self, coeffs):
        """Function to add transient term to coefficient arrays"""

        # Calculate the transient term
        Transient_term = (self._rho*self._cp*self._grid.vol)*(1.5*self._T[1:-1] - 2*self._Told[1:-1] + 0.5*Too[1:-1])/self._dt
        # Calculate the linearization coefficient
        coeffP = 1.5*(self._rho*self._cp*self._grid.vol)/self._dt
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(Transient_term)

        return coeffs

