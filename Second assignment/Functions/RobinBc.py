
# coding: utf-8

# In[ ]:


class RobinBc:
    """Class defining a Neumann boundary condition"""
    
    def __init__(self, phi, grid, ho, Tow, Toe, k, loc):
        """Constructor
            phi ........ field variable array
            grid ....... grid
            gradient ... gradient at cell adjacent to boundary
            loc ........ boundary location
        """
        self._phi = phi
        self._grid = grid
        self._ho = ho
        self._Tow = Tow
        self._Toe = Toe
        self._k = k
        self._loc = loc
        
    def value(self):
        """Return the boundary condition value"""
        if self._loc is BoundaryLocation.WEST:
            return (self._phi[1] + ((self._grid.dx_WP[0]*self._ho*self._Tow)/self._k))/(1+((self._grid.dx_WP[0]*self._ho)/self._k))
        elif self._loc is BoundaryLocation.EAST:
            return (self._phi[-2] + ((self._grid.dx_PE[-1]*self._ho*self._Toe)/self._k))/(1+((self._grid.dx_PE[-1]*self._ho)/self._k))
        else:
            raise ValueError("Unknown boundary location")
    
    def coeff(self):
        """Return the linearization coefficient"""
        if self._loc is BoundaryLocation.WEST:
            return 1 / (1+((self._grid.dx_WP[0]*self._ho)/self._k))
        elif self._loc is BoundaryLocation.EAST:
            return 1 / (1+((self._grid.dx_PE[-1]*self._ho)/self._k))
        else:
            raise ValueError("Unknown boundary location")
            
    def apply(self):
        """Applies the boundary condition in the referenced field variable array"""
        if self._loc is BoundaryLocation.WEST:
            self._phi[0] = (self._phi[1] + ((self._grid.dx_WP[0]*self._ho*self._Tow)/self._k))/(1+((self._grid.dx_WP[0]*self._ho)/self._k))
        elif self._loc is BoundaryLocation.EAST:
            self._phi[-1] = (self._phi[-2] + ((self._grid.dx_PE[-1]*self._ho*self._Toe)/self._k))/(1+((self._grid.dx_PE[-1]*self._ho)/self._k))
        else:
            raise ValueError("Unknown boundary location")

